const palabras = [
    "Arquitectura",
    "Embajador",
    "Fotosintesis",
    "Hipopotamo",
    "Murcielago",
    "Onomatopeya",
    "Quirofano",
    "Renacuajo",
    "Zoologico",
    "Xilofono"
]

//* Variables de control
let usedLetters = []
let mistakes = 0
let hits = 0

const mainElement = document.querySelector('main')
const letterContentElement = document.querySelector('.content')
const datosElement = document.querySelector('.datos')
const lettersElement = document.querySelector('.letters')
const intentosElement = document.createElement('span')
const fallosElement = document.createElement('span')
const canvas = document.querySelector('canvas')
const errorElement = document.querySelector('.error')
const spanError = document.createElement('span')
const messageError = document.createElement('p')
const btnReiniciar = document.createElement('button')

const word = palabras[Math.floor(Math.random() * palabras.length)].toUpperCase()
console.log(word)
const wordArray = word.split('')
const bodyParts = [
  [88, 52, 88, 77],// torso
  [90, 57, 105, 57], // brazo der
  [72, 57, 88, 57], // brazo izq
  [88, 76, 100, 90], // pierna der
  [75, 90, 87, 76] // pierna izq
]

const ctx = canvas.getContext('2d')
const base = [
  [15, 130, 40, 5],// base poste
  [30, 20, 5, 115], // poste izq
  [30, 20, 65, 5], // poste arriba
  [85, 20, 5, 15], // cuerda
]

const union = [32, 40, 55, 23]

ctx.fillStyle = '#000'
const drawBase = () => {
  ctx.beginPath()
  for (let baseEle of base) {
    ctx.fillRect(...baseEle)
  }
  // Dibujo diagonal
  drawPart(...union)
  ctx.closePath()
}


const drawParts = part => {
  ctx.beginPath()
  if (part === 0) drawHead()
  else {
    const [inicioX, inicioY, finX, finY] = bodyParts[part - 1]
    drawPart(inicioX, inicioY, finX, finY)
  }
  ctx.closePath()
}

const drawPart = (inicioX, inicioY, finX, finY) => {
  ctx.moveTo(inicioX, inicioY)
  ctx.lineTo(finX, finY)
  ctx.lineWidth = 3
  ctx.stroke()
}

const drawHead = () => {
  // Coordenadas del centro del círculo
  const x = 88
  const y = 43
  
  // Radio del círculo
  const radio = 8
  
  // Ángulo de inicio (0) y ángulo de fin (Math.PI * 2) para un círculo completo
  ctx.arc(x, y, radio, 0, Math.PI * 2)
  ctx.lineWidth = 3
  ctx.stroke()
}

const drawGuiones = (word) => {
  word.forEach(() => {
    const spanGuion = document.createElement('span')
    spanGuion.innerText = '_'
    letterContentElement.appendChild(spanGuion)
  })
}

const endGame = () => {
  btnReiniciar.classList.add('btnReiniciar')
  btnReiniciar.innerText = 'Reiniciar'
  errorElement.appendChild(btnReiniciar)
  document.removeEventListener('keydown', letterEvent);
  spanError.innerText = 'Game Over'
  btnReiniciar.addEventListener('click', () => document.location.reload())
}

const winGame = () => {
  errorElement.classList.add('win')
  errorElement.classList.remove('hidden')
  errorElement.classList.remove('error')
  canvas.classList.add('hidden')
  spanError.innerText = 'Ganaste'
  btnReiniciar.innerText = 'Reiniciar'
  btnReiniciar.classList.add('btnReiniciar')
  btnReiniciar.addEventListener('click', () => document.location.reload())
  errorElement.appendChild(spanError)
  errorElement.appendChild(btnReiniciar)
  mainElement.style.gap = '50px'
}

const correctLetter = letter => {
  const spanArray = document.querySelectorAll('.content>span')
  for(let i = 0; i < wordArray.length; i++) {
    if(wordArray[i] === letter) {
      spanArray[i].innerText = letter
      hits++
    }
  }
  updateAttempts()
  const frase = Array.from(spanArray).map(span => span.innerText).join('')
  if(frase === word) winGame()
}

const wrongLetter = () => {
  spanError.innerText = 'X'
  messageError.innerText = 'Intento fallido'
  errorElement.appendChild(spanError)
  errorElement.appendChild(messageError)
  errorElement.classList.remove('hidden')
  drawParts(mistakes)
  mistakes++
  hits++
  updateAttempts()
  if(mistakes === bodyParts.length + 1) endGame()
  else setTimeout(()=>errorElement.classList.add('hidden'), 2000)
}

const addLetter = letter => {
  const letterElement = document.createElement('span')
  letterElement.innerHTML = letter.toUpperCase()
  lettersElement.appendChild(letterElement)
}

// Función para verificar que la letra presiona exista en la palabra
const letterInput = letter => {
  if(word.includes(letter)) {
    correctLetter(letter)
  } else {
    wrongLetter()
    addLetter(letter)
  }
  usedLetters.push(letter)
}

const updateAttempts = () => {
  intentosElement.innerText = `Intentos: ${hits}`
  fallosElement.innerText = `Fallos: ${mistakes}`
}

const letterEvent = e => {
  errorElement.classList.add('hidden')
  if (errorElement.hasChildNodes()){
    errorElement.removeChild(spanError)
    errorElement.removeChild(messageError)
  }
  let letter = e.key.toUpperCase()
  if(letter.match(/^[a-zñ]$/i) && !usedLetters.includes(letter)) {
    letterInput(letter)
  }
}

const inicio = () => {
  errorElement.classList.add('hidden')
  intentosElement.innerText = `Intentos: ${hits}`
  fallosElement.innerText = `Fallos: ${mistakes}`
  datosElement.appendChild(intentosElement)
  datosElement.appendChild(fallosElement)
  letterContentElement.innerHTML = ''
  drawBase()
  drawGuiones(wordArray)
  updateAttempts()
  document.addEventListener('keydown', letterEvent)
}

inicio()